#!/usr/bin/python3
"""

  Usage example: 
	./mask_identity.py test_file.txt

  More info:
    https://www.regular-expressions.info/unicode.html
    https://docs.microsoft.com/en-us/dotnet/standard/base-types/character-classes-in-regular-expressions

"""
import re
import sys
import regex as reunicode
import collections
import os.path
import pandas

class Masker(object):
	def __init__(self, verbose=False, use="all"):
		self.verbose=verbose
		self.load_tools()
		self.init_pipe(use)

	def init_pipe(self, use):
		self.pipe = []
		if use == "all":
			for tool in self.toolbox:
				self.pipe.append(self.toolbox[tool])
				self.pipe.append(self.toolbox['onespace'])
		elif type(use).__name__ in ['list']:
			for tool in use:
				if tool in self.toolbox:
					self.pipe.append(self.toolbox[tool])
		else:
			raise RuntimeError("No tools were registered!")	

	def load_tools(self):
		"""
		  Register tools in the toolbox
		"""
		self.toolbox = collections.OrderedDict()
		# register tools
		self.escape()
		self.tag()
		self.name()
		self.email()
		self.digit()
		self.onespace()

	def test(self):
		"""
		  Run some simple end-to-end tests
		"""
		data = [
		  "Tere! Minu nimi on Ivo LINNA, isikukood 34512042722 ja email ivo.linna@rockhotel.ee",
		  "Hei, mida ma peaks tegema, et Tallink'is muuta oma laevabronni? Nimi on Margus Kappel ja isikukood 12134456. Olen 65 aastat vana. Tänud!",
		  "Kuidas ma saan teha nii, et minu nimi (Kalmer Tennosaar, sündinud 22/08/2014) ei ilmuks teie poolt salvestatud andmetesse?",
		]
		for x in data:
			print("Enne: %s\nPärast: %s\n" % (x, self.apply(x)))

	def digit(self):
		"""
		  Mask digits 
		"""
		myname = 'digit'
		regex = re.compile('[0-9]+')
		if self.verbose:
			sub_pattern = "__%s__" % myname
		else:
			sub_pattern = ""
		self.toolbox[myname] = lambda x: regex.sub(sub_pattern, x)

	def email(self):
		"""
		  Mask email addresses
		"""
		myname = 'email'
		regex = re.compile('[^@ ]+@[^@ ]+\.[^@ ]+')
		if self.verbose:
			sub_pattern = "__%s__" % myname
		else:
			sub_pattern = ""
		self.toolbox[myname] = lambda x: regex.sub(sub_pattern, x)

	def name(self):
		"""
		  Mask capitalized or uppercase words in the middle or end of sentence
		"""
		myname = 'name'
		# do not match ". Tere"
		regex = []
		# match capitalized words, except when preceded by .!?_
		# lookarounds: https://www.regular-expressions.info/lookaround.html
		# negative lookbehind (?<!x): https://stackoverflow.com/a/13748823
		regex.append(reunicode.compile(r'(?<![.!?_]) \p{Lu}+[-\p{Ll}]*'))
		if self.verbose:
			sub_pattern = "__%s__" % myname
		else:
			sub_pattern = ""
		def apply_regex(x):
			for r in regex:
				x = r.sub(sub_pattern, x)
			return x
		self.toolbox[myname] = apply_regex

	def tag(self):
		"""
		  09:10:50 Visitor: -> replace r"([0-9]+:)+[0-9] Visitor:" with "__visitor__"
		  09:05:43 Gunnar Graps: -> replace r"([0-9]+:)+[0-9]+ \w+ \w+:" with "__support__"
		"""
		myname = 'tag'
		regex_visitor = reunicode.compile(r"([0-9]+:)+[0-9]+ Visitor:")
		regex_support = reunicode.compile(r"([0-9]+:)+[0-9]+ \w+ \w+:")
		self.toolbox[myname] = lambda x: regex_support.sub("__support__", regex_visitor.sub("__visitor__", x))

	def escape(self):
		"""
		  List of strings to prevent from masking
		"""
		escape = ["Tall", "Tll", "Hel", "Stoc", "Marienh", "Riia", "Riiga", "Turk", "Turg", "Club", "One"]
		regex_list = []
		for e in escape:
			regex_list.append((reunicode.compile(e), e.lower()))
		def replace(x):
			for regex, sub_pattern in regex_list:
				x = regex.sub(sub_pattern, x)
			return x
		self.toolbox["escape"] = replace

	def filter(self):
		"""
		  Remove these characters
		"""
		remove_char = set([':'])
		def filter_char(x):
			return ''.join(filter(lambda x: False if x in remove_char else True, x))
		self.toolbox["filter"] = filter_char

	def onespace(self):
		"""
		  Remove multiple spaces
		"""
		regex = reunicode.compile(r"\s+")
		sub_pattern = " "
		def fun(x):
			return regex.sub(sub_pattern, x)
		self.toolbox["onespace"] = fun 

	def apply(self, x):
		"""
		  Apply the pipeline to x
		"""
		result = x
		for f in self.pipe:
			result = f(result)
		return result

def read_lines(fname):
	with open(fname) as f:
		content = f.readlines()
	result = collections.deque()
	for x in content:
		result.append(x)
	return result

def main(fname):
	fname_split = os.path.splitext(fname)
	out_file = fname_split[0] + "_masked" + fname_split[1]

	masker = Masker()
	result = collections.deque()

	if "csv" in fname:
		data = pandas.read_table(fname, sep=',')
		data = data.dropna(axis=0, subset=['chatlog'])
		data.loc[:,'chatlog'] = data['chatlog'].apply(masker.apply)
		data.to_csv(out_file, sep=",", index=None)
	else:
		data = read_lines(fname)
		for line in data:
			result.append(masker.apply(line))
		with open(out_file, 'w') as f:
			f.write(''.join(result))
	print("Output masked results to %s" % out_file)


if __name__ == "__main__":
	if len(sys.argv) < 2:
		print("Usage: mask_identity.py [filename]")
	else:
		main(sys.argv[1])

