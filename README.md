# README #

Python3 script to mask private information in text files.

### Version ###

* Version 0.1

### Usage example ###

./mask_identity.py test_file.txt

### Requirements ###

* Python3

### Questions? ###

* hl@alphablues.com
